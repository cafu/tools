#!/usr/bin/perl
# This script creates a random password for use with .htaccess/.htpasswd.

use strict;     # turn 'strict' mode on
use warnings;   # -w


sub generate_random_password
{
    my $passwordsize = shift;
    my @alphanumeric = ('a'..'z', 'A'..'Z', 0..9);
    my $randpassword = join '', map $alphanumeric[rand @alphanumeric], 0..$passwordsize;

    return $randpassword;
}


my $pwd =generate_random_password(8);
my $salt="cf";

print($pwd, "\t", crypt($pwd, $salt));
